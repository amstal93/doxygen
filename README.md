# Doxygen Docker image

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Docker Stars][docker-stars-badge]][docker-repository]
[![Docker Pulls][docker-pulls-badge]][docker-repository]

Docker image with [Doxygen][doxygen-website], based on Alpine Linux.

Image also includes [Graphviz][graphviz-website], used by Doxygen
in order to create call and inheritance graphs.

## Table of Contents

- [Tags](#tags)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Building Docker image](#building-docker-image)
- [License](#license)

## Tags

Following tags are available on [Docker Hub][docker-repository]:

| Tag name | WORKDIR    | Base image      | Doxygen | Graphviz |
|----------|------------|-----------------|---------|----------|
| `latest` | `/opt/prj` | `alpine:latest` | Latest  | Latest   |

Tags are rebuilt every week by a job scheduled on GitLab CI platform.

## Usage

Command line usage is pretty simple:

```bash
cd /path/to/project
docker run --rm -v $(pwd):/opt/prj -it pommalabs/doxygen
```

Doxygen can also be easily used as a step in GitLab CI:

```yaml
pages:
  stage: deploy
  image:
    name: pommalabs/doxygen
    entrypoint: [""]
  script:
    - doxygen
    - mv /doxygen/output/path public
  artifacts:
    paths:
      - public
```

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.
I replaced the __Install__ section with __Tags__, since I thought that it made no sense
to "install" an helper Docker image.

### Building Docker image

Docker image can be built with following command:

```bash
docker build . -f ./Dockerfile -t $DOCKER_TAG
```

Please replace `$DOCKER_TAG` with a valid tag (e.g. `doxygen`).

## License

MIT © 2020-2022 [Alessio Parma][personal-website]

[docker-pulls-badge]: https://img.shields.io/docker/pulls/pommalabs/doxygen?style=flat-square
[docker-repository]: https://hub.docker.com/r/pommalabs/doxygen
[docker-stars-badge]: https://img.shields.io/docker/stars/pommalabs/doxygen?style=flat-square
[doxygen-website]: https://www.doxygen.nl/index.html
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/docker/doxygen/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/docker/doxygen/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[graphviz-website]: https://graphviz.org/
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[personal-website]: https://alessioparma.xyz/
[project-license]: https://gitlab.com/pommalabs/docker/doxygen/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-squar
